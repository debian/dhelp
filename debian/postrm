#!/bin/sh
#
# Copyright (C) 2002 Stefan Hornburg (Racke) <racke@linuxia.de>
# Copyright (C) 2012 Georgios M. Zarkadas <gz@member.fsf.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the Free
# Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
# MA  02111-1307  USA.

set -e

#DEBHELPER#

#### Globals ####

DHELP_APTHOOK=/etc/apt/apt.conf.d/35dhelp

#### Import Library ####

# Unfortunately the library cannot be available in postrm, since all files
# (but conffiles) of the package have been already purged when it is run.
# Thus we need to manually copy all needed functions here.


# Test executability (implies existence) of a binary in the system's path.
# ARGS:
# $1=binary_basename
#
is_exec()
{
    # First ensure we are not passed an empty argument,
    # because test -x "" returns success.

    if [ "X${1}" = "X" ]; then
        return 2
    fi

    # Then look for the binary location. If not found,
    # return error, because test -x "" returns success.

    local binary=$(which "${1}" 2>/dev/null)

    if [ "X${binary}" = "X" ]; then
        return 1
    fi

    # If the binary is found, return its executable status.

    test -x "${binary}" 2>/dev/null
}

# Remove a conffile from the filesystem and the ucf and ucfr databases.
# ARGS: 
# $1=package $2=conffile
#
ucf_unregister_file ()
{
    local package="${1}"
    local conffile="${2}"
    local ext

    # the '' value is to delete the file itself
    for ext in '' '~' '%' .bak .ucf-new .ucf-old .ucf-dist; do
        rm -f "${conffile}${ext}" || true
    done
    if is_exec ucf; then
        ucf --purge "${conffile}"
    fi
    if is_exec ucfr; then
        ucfr --purge "${package}" "${conffile}"
    fi
}

#### Functions ####

#### Main script body ####

case $1 in
purge)
    # Remove and unregister with ucf our dpkg post-invoke hook.

    ucf_unregister_file dhelp ${DHELP_APTHOOK}
    ;;
*)
    ;;
esac

unset DHELP_APTHOOK || true
